import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<string> {
    console.log("🚀 ~ file: app.controller.ts:10 ~ AppController ~ findOne ~ id:", id)
    return await this.appService.fireItAll(id);
  }
}
