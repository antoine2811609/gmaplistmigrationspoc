import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {}
  // Insert the raw text content from the network response from the
  // https://www.google.com/search?tbm=map&pb... url below.
  rawInput = null;

  prepare(input) {
    console.log("🚀 ~ file: app.service.ts:13 ~ AppService ~ prepare ~ input:", input);
    // There are 5 random characters before the JSON object we need to remove
    // Also I found that the newlines were messing up the JSON parsing,
    // so I removed those and it worked.
    input = input.substring(0, input.length - 6); // remove the /*""*/
    const parsedInput = JSON.parse(input);
    const preparedForParsing = parsedInput["d"].substring(5).replace(/\n/g, '');

    const json = JSON.parse(preparedForParsing);
    const results = json[0][1].map((array) => array[14]);
    return results;
  }

  prepareLookup(data) {
    // this function takes a list of indexes as arguments
    // constructs them into a line of code and then
    // execs the retrieval in a try/catch to handle data not being present
    return function lookup(...indexes) {
      const indexesWithBrackets = indexes.reduce(
        (acc, cur) => `${acc}[${cur}]`,
        '',
      );
      const cmd = `data${indexesWithBrackets}`;
      try {
        const result = eval(cmd);
        return result;
      } catch (e) {
        return null;
      }
    };
  }

  buildResults(preparedData) {
    const results = [];
    for (const place of preparedData) {
      const lookup = this.prepareLookup(place);
      console.log("🚀 ~ file: app.service.ts:48 ~ AppService ~ buildResults ~ lookup:", lookup(9,3))

      // Use the indexes below to extract certain pieces of data
      // or as a starting point of exploring the data response.
      const result = {
        address: {
          street_address: lookup(183, 1, 2),
          city: lookup(183, 1, 3),
          zip: lookup(183, 1, 4),
          state: lookup(183, 1, 5),
          country_code: lookup(183, 1, 6),
        },
        name: lookup(11),
        tags: lookup(13),
        notes: lookup(25, 15, 0, 2),
        placeId: lookup(78),
        phone: lookup(178, 0, 0),
        coordinates: {
          long: lookup(9,2),//lookup(208, 0, 2),
          lat: lookup(9,3),//lookup(208, 0, 3),
        },
      };
      results.push(result);
    }

    return results;
  }

  async getListData(listId: string): Promise<any> {
    try {
      const response = await firstValueFrom(
        this.httpService.get(
          `https://www.google.com/search?tbm=map&hl=en&pb=!4m12!1m3!1d27113.954297167707!2d28.967224509781527!3d41.04322012500663!2m3!1f0!2f0!3f0!3m2!1i645!2i865!4f13.1!7i20!10b1!12m13!1m1!18b1!2m3!5m1!6e2!20e3!10b1!16b1!17m1!3e1!20m2!5e2!6b1!19m4!2m3!1i360!2i120!4i8!20m57!2m2!1i203!2i100!3m2!2i4!5b1!6m6!1m2!1i86!2i86!1m2!1i408!2i240!7m42!1m3!1e1!2b0!3e3!1m3!1e2!2b1!3e2!1m3!1e2!2b0!3e3!1m3!1e8!2b0!3e3!1m3!1e10!2b0!3e3!1m3!1e10!2b1!3e2!1m3!1e9!2b1!3e2!1m3!1e10!2b0!3e3!1m3!1e10!2b1!3e2!1m3!1e10!2b0!3e4!2b1!4b1!9b0!22m2!1s9skBZJ3uMPG9xc8P9OOY8A4!7e81!24m76!1m25!13m9!2b1!3b1!4b1!6i1!8b1!9b1!14b1!20b1!25b1!18m14!3b1!4b1!5b1!6b1!9b1!12b1!13b1!14b1!15b1!17b1!20b1!21b1!22b0!25b0!2b1!5m5!2b1!3b1!5b1!6b1!7b1!10m1!8e3!11m1!3e1!14m1!3b1!17b1!20m2!1e3!1e6!24b1!25b1!26b1!29b1!30m1!2b1!36b1!39m3!2m2!2i1!3i1!43b1!52b1!54m1!1b1!55b1!56m2!1b1!3b1!65m5!3m4!1m3!1m2!1i224!2i298!71b1!72m4!1m2!3b1!5b1!4b1!89b1!113b1!26m4!2m3!1i80!2i92!4i8!30m0!34m18!2b1!3b1!4b1!6b1!8m6!1b1!3b1!4b1!5b1!6b1!7b1!9b1!12b1!14b1!20b1!23b1!25b1!26b1!37m1!1e81!42b1!47m0!49m6!3b1!6m2!1b1!2b1!7m1!1e3!50m4!2e2!3m2!1b1!3b1!54m2!1e2!2${listId}!67m2!7b1!10b1!69i636&q=*&tch=1&ech=1`,
        ),
      );

      return response.data;
    } catch (error) {
      console.log(
        '🚀 ~ file: app.service.ts:82 ~ AppService ~ getListData ~ error:',
        error,
      );
    }
  }
  async fireItAll(listId: string): Promise<string> {
    console.log("firing");
    
    const listResponse = await this.getListData(listId);


    const preparedData = this.prepare(listResponse);


    const listResults = this.buildResults(preparedData);
    console.log(
      '🚀 ~ file: app.service.ts:76 ~ AppService ~ fireItAll ~ listResults:',
      listResults,
    );
    return JSON.stringify(listResults);
  }
}

